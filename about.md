---
layout: page
title: About
permalink: /about/
---

WorldsProject is aiming to lower the cost of essential items through automation and distributed technology.

The approach being taken is multipronged, with a combination of near term goals for immediate wins, to longer term goals.

Near term, both filanew and the unnamed plant system are both items that reduce the cost of both food, and small scale
manufacturing.

Longer term, Harp aims to help people utilize tools and materials they have on hand by building anything contained
within Harps globally distributed blueprint database.
