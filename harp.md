---
layout: page
title: Harp
permalink: /harp
---
Harp stands for HARdware Packager. It's a series of software packages that aims
to provide an all in once package for automating manufacturing.

This will be a distributed global blueprint system, with software hooks to control CNC machines,
instruction generation for tools that are not computer connected, and purchasing ability to aquire what
cannot be made locally.
