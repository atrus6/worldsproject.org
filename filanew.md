---
layout: page
title: Filanew
permalink: /filanew
---
Filanew is an operation to recycle used 3D Printed plastic. People can send in their
used plastic and have it returned back to them as filament for their printer.

More info can be found at: https://filanew.com
