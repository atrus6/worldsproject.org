---
layout: post
categories: update
tags: etsy
---

Worked on the electronics case for the circle knit so that it no longer
requires to be laser cut, and can now be 3D printed. Since laser cutting is
so much more expensive than 3D printing, this does save on some cost. Of course
the original files are still there, so if you feel that laser cutting is a better
option for you, it is still there.

As was reported last week, there was a decent push to add some more Etsy products.
They've been made, and will hopefully be up before the next update.

Soon, there will be some tutorials on all the products that are on the Etsy shop.
While they are simple items, one would wonder why bother putting a tutorial up for
something like that?

The answer is simple. There is an advantage in more prople having 'maker' skills.
From doing crafts, or fixing items around the house, a population that can create
is a valuable and productive one.

And simple tutorials for simple items can help someone who is new to doing things.
These simple tutorials can help give them confidence, and hopefully enable them to
explore other things.
