---
layout: post
title: "Mushrooms and Harp"
categories: Updates
tags: mushrooms harp
date: "2017-09-18 19:07:30 -0500"
---

Mushrooms are growing! Little tiny baby ones, which is pretty neat. Hopefully, they'll
be full grown, or nearly enough, by the next update.

There's been some pretty major updates to harp, with the materials having a proper
webpage. Hopefully the first print will be done by the weekend.
